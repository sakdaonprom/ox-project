/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ox;

import java.util.Scanner;

/**
 *
 * @author PC Sakda
 */
public class OX {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        char[][] table = new char[4][4];
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                table[i][j] = '-';
            }
        }

        System.out.println("Welcome to OX Game");
        

        boolean player1 = true;
        boolean gameEnd = false;

        while (!gameEnd) {
            drawTable(table);
            if (player1) {
                System.out.println("Turn O");
            } else {
                System.out.println("Turn X");
            }

            char c = '-';
            if (player1) {
                c = 'O';
            } else {
                c = 'X';
            }

            int row = 0;
            int col = 0;

            while (true) {
                row = kb.nextInt();
                col = kb.nextInt();
                System.out.println("Please input row, col:" + row + " " + col);
                if (row < 1 || col < 1 || row > 3 || col > 3) {
                    System.out.println("Error The input data set exceeds the index Try again.");
                } else if (table[row][col] != '-') {
                    System.out.println("Superimposed position Try again.");
                } else {
                    break;
                }

            }

            table[row][col] = c;
            if (CheckWin(table) == 'X') {
                drawTable(table);
                System.out.println(">>>X Win<<<");
                gameEnd = true;
            } else if (CheckWin(table) == 'O') {
                drawTable(table);
                System.out.println(">>>O Win<<<");
                gameEnd = true;
            } else {
                if (CheckDraw(table)) {
                    drawTable(table);
                    System.out.println(">>> Draw <<<");
                    gameEnd = true;
                } else {
                    player1 = !player1;
                }
            }
        }

        kb.close();

    }

    public static void drawTable(char[][] table) {

        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public static char CheckWin(char[][] board) {
        for (int i = 1; i <= 3; i++) {
            if (board[i][1] == board[i][2]
                    && board[i][2] == board[i][3]
                    && board[i][1] != '-') {
                return board[i][1];
            }
        }

        for (int j = 1; j <= 3; j++) {
            if (board[1][j] == board[2][j]
                    && board[2][j] == board[3][j]
                    && board[1][j] != '-') {
                return board[1][j];
            }
        }

        if (board[1][1] == board[2][2]
                && board[2][2] == board[3][3]
                && board[1][1] != '-') {
            return board[1][1];
        }

        if (board[3][1] == board[2][2]
                && board[2][2] == board[1][3]
                && board[3][1] != '-') {
            return board[3][1];
        }

        return ' ';
    }

    public static boolean CheckDraw(char[][] board) {
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}
